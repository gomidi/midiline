package logic

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

type registryNot struct{}

func (t registryNot) Name() string {
	return "NOT"
}

func (t registryNot) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var Condition midiline.Condition
	err := config.Parse(c, args, &Condition)
	if err != nil {
		return nil, err
	}

	return Not(Condition), nil
}

func init() {
	config.RegisterConditionMaker(registryNot{}, "condition [string]")
}

// Not returns a matcher that matches if m does not match.
func Not(m midiline.Condition) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		return !m.IsMet(msg)
	})
}

type registryOr struct{}

func (t registryOr) Name() string {
	return "OR"
}

func (t registryOr) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var matchers = make([]*midiline.Condition, len(args))
	var mt = make([]interface{}, len(args))

	for i := 0; i < len(args); i++ {
		var m midiline.Condition
		mm := &m
		matchers[i] = mm
		mt[i] = mm
	}
	err := config.Parse(c, args, mt...)
	if err != nil {
		return nil, err
	}

	var _matchers = make([]midiline.Condition, len(args))
	for i := 0; i < len(args); i++ {
		_matchers[i] = *matchers[i]
	}

	return Or(_matchers...), nil
}

func init() {
	config.RegisterConditionMaker(registryOr{}, "condition1 [string], condition2 [string],...")
}

func Or(ma ...midiline.Condition) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		for _, m := range ma {
			if m.IsMet(msg) {
				return true
			}
		}
		return false
	})
}

type registryAnd struct{}

func (t registryAnd) Name() string {
	return "AND"
}

func (t registryAnd) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var matchers = make([]*midiline.Condition, len(args))
	var mt = make([]interface{}, len(args))

	for i := 0; i < len(args); i++ {
		var m midiline.Condition
		mm := &m
		matchers[i] = mm
		mt[i] = mm
	}
	err := config.Parse(c, args, mt...)
	if err != nil {
		return nil, err
	}

	var _matchers = make([]midiline.Condition, len(args))
	for i := 0; i < len(args); i++ {
		_matchers[i] = *matchers[i]
	}

	return And(_matchers...), nil
}

func init() {
	config.RegisterConditionMaker(registryAnd{}, "condition1 [string], condition2 [string],...")
}

func And(ma ...midiline.Condition) midiline.Condition {
	return midiline.ConditionFunc(func(msg midi.Message) bool {
		for _, m := range ma {
			if !m.IsMet(msg) {
				return false
			}
		}
		return true
	})
}
