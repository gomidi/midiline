package typ

import (
	"testing"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midi/midimessage/meta"
	"gitlab.com/gomidi/midi/midimessage/sysex"
)

func TestMatches(t *testing.T) {

	tests := []struct {
		filter   Type
		name     string
		msg      midi.Message
		expected bool
	}{
		{SysExMsg, "SystemExclusiveMsg", sysex.SysEx([]byte{23}), true},
		{SysCommonMsg, "SystemCommonMsg", sysex.SysEx([]byte{23}), false},
		{MetaMsg, "MetaMsg", sysex.SysEx([]byte{23}), false},
		{ChannelMsg, "ChannelMsg", sysex.SysEx([]byte{23}), false},
		{RealtimeMsg, "RealtimeMsg", sysex.SysEx([]byte{23}), false},
		{NoteMsg, "NoteMsg", sysex.SysEx([]byte{23}), false},
		{PitchbendMsg, "PitchbendMsg", sysex.SysEx([]byte{23}), false},
		{ControlChangeMsg, "ControlChangeMsg", sysex.SysEx([]byte{23}), false},

		{NoteMsg | ChannelAny, "NoteMsg | ChannelAny", channel.Channel0.NoteOn(20, 120), true},
		{NoteMsg | Channel0, "NoteMsg | Channel0", channel.Channel0.NoteOn(20, 120), true},
		{NoteMsg, "NoteMsg", channel.Channel0.NoteOn(20, 120), false},

		{NoteMsg | Channel1, "NoteMsg | Channel1", channel.Channel0.NoteOn(20, 120), false},

		{PitchbendMsg | ChannelAny, "PitchbendMsg | ChannelAny", channel.Channel0.Pitchbend(12), true},
		{ChannelMsg | ChannelAny, "ChannelMsg | ChannelAny", channel.Channel0.Pitchbend(12), true},
		{ChannelMsg | Channel0, "ChannelMsg | Channel0", channel.Channel0.Pitchbend(12), true},
		{ChannelMsg | Channel1, "ChannelMsg | Channel0", channel.Channel0.Pitchbend(12), false},
		{PitchbendMsg | Channel0, "PitchbendMsg | Channel0", channel.Channel0.Pitchbend(12), true},
		{PitchbendMsg | Channel1, "PitchbendMsg | Channel1", channel.Channel0.Pitchbend(12), false},

		{ChannelMsg | ChannelAny, "ChannelMsg | ChannelAny", meta.Text("hello"), false},
	}

	for _, test := range tests {

		if got, want := test.filter.IsMet(test.msg), test.expected; got != want {
			t.Errorf("(%#v).IsMet(%s) = %v; want %v", test.name, test.msg, got, want)
		}
	}

}
