# midiline
A manipulation line for MIDI data

[![Build Status Travis/Linux](https://travis-ci.org/gomidi/midiline.svg?branch=master)](http://travis-ci.org/gomidi/midiline) [![Documentation](http://godoc.org/github.com/gomidi/midiline?status.png)](http://godoc.org/github.com/gomidi/midiline)

## Status

proof of concept

## Installation

```sh
go get -d github.com/gomidi/midiline/...
```

## Example

```go
package main

import (
	"github.com/gomidi/midiline"
	"github.com/gomidi/midiline/actions/channelchange"
	"github.com/gomidi/midiline/actions/to_cc"
	"github.com/gomidi/midiline/conditions/logic"
	"github.com/gomidi/midiline/conditions/message"
	"github.com/gomidi/midiline/conditions/typ"
	"github.com/gomidi/midiline/value"
	"github.com/gomidi/rtmididrv"
)

func check(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func main() {

	// create a new MIDI line
	line := midiline.New("my line",

		// pass...
		midiline.Pass(
			// ...all note messages on channel 1 within the key range from 12 to 50, ignoring noteoff messages
			logic.And(typ.Channel1, message.NoteKeyRange(12, 50, true)),

			// ...to the action that converts note messages to controller 12 messages
			// by taking the key of the note as value for the controller
			to_cc.Action(12, value.NoteKey()),
		),

		// then change the channel of every channel 1 message to channel 8
		channelchange.Action(1, 8),
	)

	drv, err := rtmididrv.New()
	check(err)
	defer drv.Close()
	ins, err := drv.Ins()
	check(err)
	outs, err := drv.Outs()
	check(err)

	// make a live connection from in port to out port
	ctx := midiline.Connect(ins[0], outs[0])

	// start the connection and pipe the message through the line (the transformed messages will arrive at out)
	stop, err := ctx.Run(line)
	check(err)

	// stop the runner (can only be called once)
	stop <- true
}
```