package midiline

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/gomidi/midi"
)

// Action acts on MIDI messages.
type Action interface {

	// Perform acts on the given message and returns the result.
	// To allow note producing transformers,
	// Perform is called on every tick and
	// when there is no message on the tick, the message is LineTick.
	// This way the action can return messages on every tick
	// (if it wants to). Use case would be a looper or a metronome.
	Perform(Context, midi.Message) midi.Message
}

type ActionFunc func(Context, midi.Message) midi.Message

func (f ActionFunc) Perform(ctx Context, in midi.Message) midi.Message {
	return f(ctx, in)
}

type DuplicatedLineError string

func (e DuplicatedLineError) Error() string {
	return fmt.Sprintf("ERROR: there are two lines with the name %v or this line has been embedded twice", string(e))
}

func (e DuplicatedLineError) LineName() string {
	return string(e)
}

type LineEmbeddingLoopError string

func (e LineEmbeddingLoopError) Error() string {
	return fmt.Sprintf("ERROR: loop in line embedding: %#v", string(e))
}

func (e LineEmbeddingLoopError) LoopPath() string {
	return string(e)
}

// Line has a slice of actions that will be called in order when the Run method is called.
type Line struct {
	name    string
	actions []Action
}

// New returns a new Line for the given Actions.
func New(name string, acts ...Action) *Line {
	return &Line{name, acts}
}

func (l *Line) Name() string {
	return l.name
}

func (l *Line) String() string {
	return l.name
}

func (l *Line) hasOrIsOneOfLines(path []string, ll []*Line) (loopPath []string, has bool) {
	path = append(path, l.name)
	for _, _ll := range ll {
		if reflect.DeepEqual(l, _ll) {
			return path, true
		}
	}
	for _, act := range l.actions {
		if _emb, is := act.(*embeddedLine); is {
			pa, incl := _emb.st.hasOrIsOneOfLines(path, append(ll, l))
			if incl {
				return pa, true
			}
		}
	}
	return path, false
}

// Check checks, if we got an infinite loop within a Line.
func (l *Line) Check() error {
	p, inc := l.hasOrIsOneOfLines([]string{}, nil)
	if inc {
		return LineEmbeddingLoopError(strings.Join(p, " -> "))
	}
	return nil
}

func (st *Line) runAction(currentMessages []midi.Message, ct Context, tr Action) (newMsgs []midi.Message) {

	for _, msg := range currentMessages {
		m := tr.Perform(ct, msg)
		if m == nil {
			continue
		}

		if mm, isMulti := m.(MultiMessage); isMulti {
			for _, mmm := range mm {
				if mmm == nil {
					continue
				}
				newMsgs = append(newMsgs, mmm)
			}
		} else {
			newMsgs = append(newMsgs, m)
		}
	}
	return newMsgs
}

func (st *Line) _runThroughLine(ctx Context, msg midi.Message) []midi.Message {
	currentMessages := []midi.Message{msg}

	for _, h := range st.actions {
		currentMessages = st.runAction(currentMessages, ctx, h)
	}

	return currentMessages
}

type embeddedLine struct {
	st *Line
	// flags filter.Filter
}

func (e *embeddedLine) Perform(ctx Context, in midi.Message) (out midi.Message) {
	currentMessages := []midi.Message{in}

	for _, h := range e.st.actions {
		currentMessages = e.st.runAction(currentMessages, ctx, h)
	}

	return MultiMessage(currentMessages)
}

// Embed returns a Transformer that proxy its transformation
// to MIDITransformer that are part of the Stack
func (st *Line) Embed() Action {
	return &embeddedLine{st: st}
}

var _ Action = &embeddedLine{}
