package midiline

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
)

/*
we would like to use the stack in the following situations:

- play an instrument and pass the data through as live performance.
  wr: mid.OutConnection, rd: mid.InConnection, scheduling via timestamp
- record playing of an instrument (probably better done via transformer)
  here we need scheduling via timestamps and transforming this timestamp
  to ticks back again
- reading a MIDI file and playing it in realtime. Here we need tick timestamp
  translation in the reader, i.e. every midi.Message will be a scheduled message.
- reading a MIDI file and transforming it an saving it to another one.
  here we would need the ticks and no timestamps at all. it would be important to
  have/count the bars and the beats in this case to be able to see where we are (e.g.)
  to be able to change stuff at a certain. however also for live performances, it could be
  cool to have the beat and the bar to e.g. trigger a certain note on the fourth beat within
  the bar or every 2nd bar on the first beat. but for that to work, we would definitively
  need to have some definition of tempo and beat when starting the live performance and some
  way to change it, while it is running. also to have a chance to understand what we are doing,
  some midi metronome would be cool and some output of the current bar number.

So in sum:

  - it would good to get the current bar and current beat within the bar from the context, no
    matter live or not
  - for live we would need a way to have the initial tempo and time signature and a way to change
    tempo and time signature on the fly (say time signature on the next bar and tempo immediately)
    (that would also allow us to have loopers as transformers, maybe)
  - for writing of smf files we would need the absolute tick and the current track. that means, we
    also need a current track when doing live and a way to switch to the next track (interesting side
    effect: tracks could be used as different "takes" in a loop)
  - in order to do looping like thing live, we need a steady time to invoke the Transformers no matter what.
    (it could make sense to have it bound to the minimum resolution, i.e. every tick, all transformers are
    called, then they all have to check, if there is any data (which is ugly), but they would have the
    opportunity to create data out of nothing)
  - this out of nothing could also be a SMF file, which leads to the question, if it wouldn't be a good
    idea to implement smf reading as a transformer.
  - in each case we also need a faster than realtime solution for modification of an SMF.
  - perhaps the Transformer that need to be called on every tick must fullfill an optional second interface
    by providing an EachTick() method. Then we could do things like switching between MIDI loops with the
    keyboard while having the loops be played on time (key switch is cached within the Transformer and still
    available on the right tick)
  - then the question is, if we need scheduling at all. when the transformer knows the current absolute tick
    and the deltatick for some musical time and is invoked on every tick, then it could as well do the
    caching on its own and check, if it has a tick that is to be delivered. there could some helpers been made for that
  - in this case we would need no stinking timestamp and no gorouting for writing and would simply write
    to the writer as fast as we can.
  - so to sum up, we need a reader that always delivers ticks. If its got realtime input it should block
    until the next tick (which it has to calculate) and if its got SMF input it should pass the tick as
    fast as it can. the stack is polling the reader, gets the next tick and runs it through the stack.
  - at the end of the stack there might be some transformer that writes either to a file or to live output.
    no matter what, it writes the data as fast as it gets them.
  - the only problem is when playing an SMF file: it would be played to fast because the reader reads in
    as fast as he can and the player plays as fast as he can. in this case we would need a wrapper around
    the reader that just sleeps the duration of the tick before unblocking.
  - tempo changes and timesignature changes could be wrapped/escaped within sysex message (so could be karaoke
    text for live display), also could tempo changes be signalled through midi time code. also we've got
    the realtime tick message. what about that? do we want to follow it, or send it?

    MIDI tick: System Realtime
    Some master device that controls sequence playback sends this timing message to keep a slave device in
    sync with the master. A MIDI Tick message is sent at regular intervals of one message every
    10 milliseconds.

    If a live reader would send a midi tick every 10 milliseconds, then it would be enough to just invoke
    the stack on messages. the tick number would be passed by the reader either way. But then is the question:
    how can this work with the callback based reading approach of rtmidi? we would need a separate
    gorouting just for sending the ticks over a channel and another on for sending the ticks via the callback
    from the channel and another goroutine to merge them together. then 10 milliseconds  would be our finest
    resolution for starting loops etc. but there might be a delay to the actual groove coming in via rtmidi.
    but frankly that can always be: the drum machine can't guess when you are playing, so one would need
    to accomodate the playing to the hearing.

    But we probably should not take the 10ms tick as finest resolution when recording to an SMF.
    Therefor we would need some translation between SMF ticks and midi ticks and we would need it anyway,
    because midi ticks are time based and SMF ticks are metric based and in between is the tempo.

    we could get the beat and the tempo via MIDI clock (that would also allow for synchronization, yeah!)
    and that would be needed to be send after a MIDI Start message.

    ok, I thing we need another thing that is not a reader and not a writer and not a transformer.
    lets call it a context (it is some how related to a context however).

    So lets say, the live reader provides us with the nice MIDI start, MIDI clock and some MIDI ticks,
    the we could use a PerformContext to get the tempo, bars and beats out of the MIDI clock and calculate
    the absolute (real) ticks based on the tempo and our ticks per quarternote resolution.
    It then passes the data as fast as possible for any transformer and writer to consume.

    Now if we have a SMF reader, we could combine it with two different contexts, depending on what we want.
    If we want to play the SMF we would choose the PlayContext who assumes SMF input, passes tempo and
    absolute ticks through, removes meta messages, might optionally generate MIDI clock, MIDI ticks and the like and sleeps for the time
    that corresponds to a tick before letting the data pass through. (we are just taking about SMF0 here, because SMF1
    would need routing to output ports and the whole file would need to be parsed ahead, but maybe there could be
    a way to have a stack per track and let them run in parallel, after pre parsing the SMF1).
    This way a player would just play when the data arrives, but also a SMF0 writer would just write that slowly.

    On the other hand we could combin the SMF reader with a SMFContext, that just passes counts the beats
    and bars and passes the tempo and time signature through it eould keep all meta messages and generate no
    realtime and system common messages.

    So in order to run your stack, you would pass:
    - a reader
    - a writer
    - and a Context
    to the Run method.

    as reader you would have the choice between
    - SMFReader, live reader
    for the preparer you would have the choice between
    - SMFContext, PerformContext and PlayContext
    for the writer you would have the choice between
    - SMFWriter, live writer

    However, not all combinations make sence

    | reader         | writer         | context         | purpose                          |
    ----------------------------------------------------------------------------------------
    | SMFReader      | SMFWriter      | SMFContext      | modifying SMF on the fly         |
    | live reader    | live writer    | PerformContext  | modifying play on the fly        |
    | SMFReader      | live writer    | PlayContext     | playing an SMF file              |
    | live reader    | SMFWriter      | PerformContext  | record the performance           |
    | SMFReader      | SMFWriter      | PerformContext  | stupid: no realtime messages to look for |
    | SMFReader      | SMFWriter      | PlayContext     | stupid: no sense, no realtime messages to look for, too slow |
    | live reader    | live writer    | PlayContext     | stupid: no sense, playing already in time        |
    | live reader    | live writer    | SMFContext      | stupid: no sense, no SMF data      |
    | SMFReader      | live writer    | SMFContext      | stupid: no SMF target            |
    | SMFReader      | live writer    | PerformContext  | stupid: no realtime messages to look for |
    | live reader    | SMFWriter      | SMFContext      | stupid: no sense, no SMF data |
    | live reader    | SMFWriter      | PlayContext     | stupid: SMF data prepared for SMFWriter |


    So it looks as if there are just a handful of reasonable use cases with specific combinations,
    so it makes sense to provide dedicated functions to just setup there use cases with stacks:

    Play(smffile, mid.OutConnection)
    Record(mid.InConnection,smffile)
    Perform(mid.InConnection, mid.OutConnection)
    PerformAndRecord(mid.InConnection, mid.OutConnection, smffile)
    // something like PerformAndPlay would need two stacks and then mix the results together
    Update(fromsmffile, tosmffile)
    // UpdateAndPlay would need two stacks from the same input, would be doable

    however in each function the in and outs could as well be midi.Readers and midi.Writers
    with the corresponding data. However that are just the most common use cases.

    The other question is: what would a Context look like?

    type Context interface {
      // Resolution returns the ticks of a quarternote
      Resolution() uint32

      // Tick returns the current absolute tick since the beginning
      Tick() uint32

      // Bar returns the current bar number
      Bar() uint32

      // Beat returns the current beat
      Beat() uint32

      // Tempo returns the current tempo
      // needed for SMF writers
      Tempo() uint32

      // TimeSignature returns the current time signature
      // needed for SMF writers
      TimeSignature()
    }




    So lets say, the reader would start by sending MIDI start, and then MIDI clock and MIDI ticks all mixed
    with the messages of the input port.

    the MIDI ticks would just be used to trigger the stack and ignored by any writer, except maybe some writer
    that would like to write sync messages anyway.



    the MIDI clock would be translated to the tempo and based on this the "real" absolute ticks based on
    a resolution in ticks per quarternote. that would also be done by the reader (I hope). the midi clock
    would also be used to count bars and beats.
    These absolute ticks could then be used by transformer middleware to do their scheduling and a SMF writer
    to do their writing.


    he message that controls the playback rate (ie, ultimately tempo) is MIDI Clock. This is sent by the master at a rate dependent upon the master's tempo. Specifically, the master sends 24 MIDI Clocks, spaced at equal intervals, during every quarter note interval.(12 MIDI Clocks are in an eighth note,
*/

// Run listens on the given midi in connection. For each arriving
// message the internal context is updated and each Handler of the Stack is
// called and may modify the message. The returned result is then consolidated (no duplicate note on / offs)
// and written to the writer (which may be an midi out connection).
// wr can be anything e.g. wr := mid.WriteTo(mid.OutConnection)
// Run returns a reader. Call any read method you want on.
// E.g. to read from a connection, call rd.ReadFrom(mid.InConnection)
// or to read from a SMF file, call rd.ReadSMFFile(file)
//func (st *Stack) Run(wr midi.Writer, opts ...Option) (rd *mid.Reader) {

// Connect returns a Runner that reads MIDI messages from the in port and writes them to the out port
// after they have been processed by the stack.
func Connect(in midi.In, out midi.Out, opts ...PerformOption) Runner {
	return newPerformanceCtx(in, out, opts...)
}

func ConnectSlim(in midi.In, out midi.Out, opts ...mid.ReaderOption) Runner {
	return newPerformanceCtxSlim(in, out, opts...)
}

// Link returns a Runner that reads MIDI messages from the SMF inFile writes them to the SMF outFile, after
// they have been processed by the stack.
func Link(inFile, outFile string, opts ...mid.ReaderOption) Runner {
	return newSMFCtx(inFile, outFile, opts...)
}

// Play returns a Runner that reads MIDI messages from the SMF inFile and writes them to the out port by respecting
// the correct timing (playing style). The messages have been processed by the stack before they reach the out port.
func Play(inFile string, out midi.Out, opts ...mid.ReaderOption) Runner {
	return newPlayCtx(inFile, out, opts...)
}
