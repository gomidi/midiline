package midiline

import (
	"gitlab.com/gomidi/midi"
)

type Context interface {
	// Resolution returns the ticks of a quarternote
	Resolution() uint32

	// Tick returns the current absolute tick since the beginning
	Tick() uint64

	// Track returns the current track
	// needed for SMF writers
	Track() uint32

	// Bar returns the current bar number
	Bar() uint32

	// Beat returns the current beat in 16ths distances of the start of the current bar
	Beat() uint8

	// TempoBPM returns the current tempo in fractional BPM
	// needed for SMF writers
	TempoBPM() float64

	// TimeSignature returns the current time signature
	// needed for SMF writers
	TimeSig() (num, denom uint8)

	// Schedule schedules the given message deltaticks in the future from now
	Schedule(deltaTicks uint32, msg midi.Message)
}

type Runner interface {
	// Run starts the engines and returns a stop channel
	// that a stop message can be send to and an error if it could not run successfully
	Run(*Line) (stop chan<- bool, err error)
}
