package midiline

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midi/midimessage/meta"
	"gitlab.com/gomidi/midi/midimessage/sysex"
)

// playCtx is used to read in SMF file and "play" it live by sending the corresponding
// midi messages at the right time. Currently only SMF0 files are supported.
type playCtx struct {
	*smfCtx
	out    midi.Out
	inFile *stopableReader
	stop   chan bool
	stdout io.Writer
	stderr io.Writer
}

/*
If a live reader would send a midi tick every 10 milliseconds, then it would be enough to just invoke
    the stack on messages. the tick number would be passed by the reader either way. But then is the question:
    how can this work with the callback based reading approach of rtmidi? we would need a separate
    gorouting just for sending the ticks over a channel and another on for sending the ticks via the callback
    from the channel and another goroutine to merge them together. then 10 milliseconds  would be our finest
    resolution for starting loops etc. but there might be a delay to the actual groove coming in via rtmidi.
    but frankly that can always be: the drum machine can't guess when you are playing, so one would need
    to accomodate the playing to the hearing.

    But we probably should not take the 10ms tick as finest resolution when recording to an SMF.
    Therefor we would need some translation between SMF ticks and midi ticks and we would need it anyway,
    because midi ticks are time based and SMF ticks are metric based and in between is the tempo.

    we could get the beat and the tempo via MIDI clock (that would also allow for synchronization, yeah!)
    and that would be needed to be send after a MIDI Start message.

    ok, I thing we need another thing that is not a reader and not a writer and not a transformer.
    lets call it a context (it is some how related to a context however).

    So lets say, the live reader provides us with the nice MIDI start, MIDI clock and some MIDI ticks,
    the we could use a PerformContext to get the tempo, bars and beats out of the MIDI clock and calculate
    the absolute (real) ticks based on the tempo and our ticks per quarternote resolution.
    It then passes the data as fast as possible for any transformer and writer to consume.

    Now if we have a SMF reader, we could combine it with two different contexts, depending on what we want.
    If we want to play the SMF we would choose the PlayContext who assumes SMF input, passes tempo and
    absolute ticks through, removes meta messages, might optionally generate MIDI clock, MIDI ticks and the like and sleeps for the time
    that corresponds to a tick before letting the data pass through. (we are just taking about SMF0 here, because SMF1
    would need routing to output ports and the whole file would need to be parsed ahead, but maybe there could be
    a way to have a stack per track and let them run in parallel, after pre parsing the SMF1).
    This way a player would just play when the data arrives, but also a SMF0 writer would just write that slowly.

    On the other hand we could combin the SMF reader with a SMFContext, that just passes counts the beats
    and bars and passes the tempo and time signature through it eould keep all meta messages and generate no
    realtime and system common messages.

    So in order to run your stack, you would pass:
    - a reader
    - a writer
    - and a Context
    to the Run method.

    as reader you would have the choice between
    - SMFReader, live reader
    for the preparer you would have the choice between
    - SMFContext, PerformContext and PlayContext
    for the writer you would have the choice between
    - SMFWriter, live writer

    However, not all combinations make sence

    | reader         | writer         | context         | purpose                          |
    ----------------------------------------------------------------------------------------
    | SMFReader      | SMFWriter      | SMFContext      | modifying SMF on the fly         |
    | live reader    | live writer    | PerformContext  | modifying play on the fly        |
    | SMFReader      | live writer    | PlayContext     | playing an SMF file              |
    | live reader    | SMFWriter      | PerformContext  | record the performance           |
    | SMFReader      | SMFWriter      | PerformContext  | stupid: no realtime messages to look for |
    | SMFReader      | SMFWriter      | PlayContext     | stupid: no sense, no realtime messages to look for, too slow |
    | live reader    | live writer    | PlayContext     | stupid: no sense, playing already in time        |
    | live reader    | live writer    | SMFContext      | stupid: no sense, no SMF data      |
    | SMFReader      | live writer    | SMFContext      | stupid: no SMF target            |
    | SMFReader      | live writer    | PerformContext  | stupid: no realtime messages to look for |
    | live reader    | SMFWriter      | SMFContext      | stupid: no sense, no SMF data |
    | live reader    | SMFWriter      | PlayContext     | stupid: SMF data prepared for SMFWriter |


    So it looks as if there are just a handful of reasonable use cases with specific combinations,
    so it makes sense to provide dedicated functions to just setup there use cases with stacks:

    Play(smffile, mid.OutConnection)
    Record(mid.InConnection,smffile)
    Perform(mid.InConnection, mid.OutConnection)
    PerformAndRecord(mid.InConnection, mid.OutConnection, smffile)
    // something like PerformAndPlay would need two stacks and then mix the results together
    Update(fromsmffile, tosmffile)
    // UpdateAndPlay would need two stacks from the same input, would be doable
*/

type stopableReader struct {
	rd      io.Reader
	stopped bool
	closed  bool
}

func (s *stopableReader) Close() error {
	if s.closed || s.rd == nil {
		return io.EOF
	}
	if cl, is := s.rd.(io.ReadCloser); is {
		cl.Close()
	}
	s.closed = true
	return nil
}

func (s *stopableReader) Read(b []byte) (int, error) {
	if s.closed {
		return 0, io.EOF
	}
	if s.stopped {
		s.Close()
		return 0, io.EOF
	}
	if s.rd == nil {
		return 0, io.EOF
	}
	n, err := s.rd.Read(b)
	if err == io.EOF {
		s.Close()
	}
	return n, err
}

var _ io.ReadCloser = &stopableReader{}

func newPlayCtx(inFile string, out midi.Out, opts ...mid.ReaderOption) *playCtx {
	c := newSMFCtx(inFile, "", opts...)
	return &playCtx{smfCtx: c, out: out, stdout: os.Stdout, stderr: os.Stderr}
}

func (ctx *playCtx) waitForStop() {
	for {
		select {
		case <-ctx.stop:
			fmt.Fprintln(ctx.stdout, "closing play context")
			//			fmt.Println("closing play context")
			ctx.inFile.Close()
			ctx.smfCtx.sleepAfterTick = false
			close(ctx.stop)
			return
		}
	}
}

// Start starts the reading and writing
func (ctx *playCtx) Run(st *Line) (stop chan<- bool, err error) {
	err = st.Check()
	if err != nil {
		return nil, fmt.Errorf("can't run play, because an error happened during the line check: %v", err)
	}
	ctx.stop = make(chan bool)
	ctx.smfCtx.st = st

	header, err := ctx.smfCtx.runHeader()
	if err != nil {
		return ctx.stop, err
	}
	_ = header

	ctx.smfCtx.sleepAfterTick = true
	ctx.smfCtx.wr = &playWriter{wr: mid.ConnectOut(ctx.out), ignoreSysex: true}
	// ctx.wr = newSmfWriter(mid.NewSMF(file, header.NumTracks, opts...))
	ctx.smfCtx.rd = mid.NewReader(ctx.smfCtx.opts...)
	ctx.smfCtx.rd.Msg.Each = ctx.smfCtx.gotMessage
	f, err := os.Open(ctx.smfCtx.inFile)
	if err != nil {
		return ctx.stop, err
	}
	ctx.inFile = &stopableReader{rd: f}
	go ctx.waitForStop()
	go func() {
		ctx.smfCtx.rd.ReadAllSMF(ctx.inFile)
	}()
	return ctx.stop, err
}

type playWriter struct {
	wr          *mid.Writer
	ignoreSysex bool
}

func (p *playWriter) writeMessage(ctx Context, msg midi.Message) {
	if msg == nil {
		panic("no nil messages allowed, use StackTick instead")
	}

	if msg == LineTick {
		return
	}

	switch msg.(type) {
	case meta.Message:
		return
	case sysex.Message:
		if !p.ignoreSysex {
			p.wr.Write(msg)
		}
		return
	default:
		p.wr.Write(msg)
	}
}
