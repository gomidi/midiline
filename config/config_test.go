package config

import (
	"encoding/json"
	"testing"
)

const jsonWants = `
{
  "int": 4,
  "float32": 2.4,
  "float64": 2.6,
  "string": "hello world!",
  "bool": true 
}
`

type testwants struct {
	int
	float32
	float64
	string
	bool
}

func TestWants(t *testing.T) {
	var m = map[string]interface{}{}

	err := json.Unmarshal([]byte(jsonWants), &m)
	if err != nil {
		t.Fatal(err)
	}

	var args = []interface{}{
		m["int"], m["float32"], m["float64"], m["string"], m["bool"],
	}

	wants := testwants{}

	err = Parse(nil, args, &wants.int, &wants.float32, &wants.float64, &wants.string, &wants.bool)

	if err != nil {
		t.Fatal(err)
	}

	if wants.int != 4 {
		t.Errorf("wrong int, wanted: 4, got: %v", wants.int)
	}

	if wants.float32 != 2.4 {
		t.Errorf("wrong float32, wanted: 2.4, got: %v", wants.float32)
	}

	if wants.float64 != 2.6 {
		t.Errorf("wrong float64, wanted: 2.6, got: %v", wants.float64)
	}

	if wants.string != "hello world!" {
		t.Errorf("wrong string, wanted: \"hello world!\", got: %#v", wants.string)
	}

	if wants.bool != true {
		t.Errorf("wrong bool, wanted: true, got: %v", wants.bool)
	}

}
