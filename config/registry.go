package config

import "gitlab.com/gomidi/midiline"

type embedReg struct{}

func (e embedReg) Name() string {
	return "line.embed"
}

func (e embedReg) Action(c Configuration, args []interface{}) (midiline.Action, error) {
	var line midiline.Line
	err := Parse(c, args, &line)
	if err != nil {
		return nil, err
	}
	return line.Embed(), nil
}

type passReg struct{}

func (e passReg) Name() string {
	return "action.pass"
}

func (e passReg) Action(c Configuration, args []interface{}) (midiline.Action, error) {
	var crit midiline.Condition
	var act midiline.Action
	err := Parse(c, args, &crit, &act)
	if err != nil {
		return nil, err
	}

	return midiline.Pass(crit, act), nil
}

type blockReg struct{}

func (e blockReg) Name() string {
	return "action.block"
}

func (e blockReg) Action(c Configuration, args []interface{}) (midiline.Action, error) {
	var crit midiline.Condition
	var act midiline.Action
	err := Parse(c, args, &crit, &act)
	if err != nil {
		return nil, err
	}

	return midiline.Block(crit, act), nil
}

func init() {
	RegisterActionMaker(embedReg{}, "line [string]")
	RegisterActionMaker(passReg{}, "condition [string], action [string]")
	RegisterActionMaker(blockReg{}, "condition [string], action [string]")
}

type ConditionMaker interface {
	Name() string
	Condition(c Configuration, args []interface{}) (midiline.Condition, error)
}

type ActionMaker interface {
	Name() string
	Action(c Configuration, args []interface{}) (midiline.Action, error)
}

func RegisterConditionMaker(m ConditionMaker, infoArgs string) {
	conditionMakerRegistry[m.Name()] = m
	conditionMakerRegistryArgs[m.Name()] = infoArgs
}

func RegisterActionMaker(t ActionMaker, infoArgs string) {
	actionMakerRegistry[t.Name()] = t
	actionMakerRegistryArgs[t.Name()] = infoArgs
}

type Configuration interface {
	FindCondition(name string) (midiline.Condition, error)
	FindAction(name string) (midiline.Action, error)
	FindLine(name string) (*midiline.Line, error)
}

// RegisteredConditionMaker returns the names of the registered Condition makers
func RegisteredConditionMaker() (names []string) {
	for name := range conditionMakerRegistry {
		names = append(names, name)
	}
	return
}

// RegisteredConditionMakerInfos returns the infos of the registered Conditions makers
func RegisteredConditionMakerInfos() (infos map[string]string) {
	infos = map[string]string{}
	for name := range conditionMakerRegistry {
		infos[name] = conditionMakerRegistryArgs[name]
	}
	return
}

// RegisteredActionMaker returns the names of the registered action makers
func RegisteredActionMaker() (names []string) {
	for name := range actionMakerRegistry {
		names = append(names, name)
	}
	return
}

// RegisteredActionMakerInfos returns the infos of the registered action makers
func RegisteredActionMakerInfos() (infos map[string]string) {
	infos = map[string]string{}
	for name := range actionMakerRegistry {
		infos[name] = actionMakerRegistryArgs[name]
	}
	return
}

var conditionMakerRegistryArgs = map[string]string{}
var conditionMakerRegistry = map[string]ConditionMaker{}

var actionMakerRegistryArgs = map[string]string{}
var actionMakerRegistry = map[string]ActionMaker{}
