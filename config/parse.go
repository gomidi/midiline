package config

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/gomidi/midiline"
	//	_ "github.com/gomidi/midiline/value"
)

func ToInt(f interface{}) (val int, ok bool) {
	fl, is := f.(float64)
	if !is {
		return 0, false
	}

	vl := int(math.Round(fl))

	if vl != int(fl) {
		return 0, false
	}
	return vl, true
}

func ToInts(f interface{}) (val []int, ok bool) {
	fl, is := f.([]float64)
	if !is {
		return nil, false
	}

	for _, f := range fl {
		vl := int(math.Round(f))

		if vl != int(f) {
			return nil, false
		}
		val = append(val, vl)
	}

	return val, true
}

func wantInt(argPos int, arg interface{}) (val int, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = ToInt(arg)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be int", argPos)
		return
	}
	return
}

func wantInts(argPos int, arg interface{}) (val []int, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = ToInts(arg)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be []int", argPos)
		return
	}
	return
}

func wantString(argPos int, arg interface{}) (val string, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = arg.(string)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be string", argPos)
		return
	}
	return
}

func wantBool(argPos int, arg interface{}) (val bool, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = arg.(bool)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be bool", argPos)
		return
	}
	return
}

func wantStrings(argPos int, arg interface{}) (val []string, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = arg.([]string)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be []string", argPos)
		return
	}
	return
}

func wantBools(argPos int, arg interface{}) (val []bool, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = arg.([]bool)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be []bool", argPos)
		return
	}
	return
}

func wantFloat(argPos int, arg interface{}) (val float64, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	val, ok := arg.(float64)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be float", argPos)
		return
	}
	return
}

func wantFloats(argPos int, arg interface{}) (val []float64, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = arg.([]float64)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be []float", argPos)
		return
	}
	return
}

func wantTime(argPos int, arg interface{}) (val time.Time, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = arg.(time.Time)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be time", argPos)
		return
	}
	return
}

func wantTimes(argPos int, arg interface{}) (val []time.Time, err error) {
	if arg == nil {
		err = fmt.Errorf("missing parameter at position %d", argPos)
		return
	}
	var ok bool
	val, ok = arg.([]time.Time)
	if !ok {
		err = fmt.Errorf("parameter at position %d must be []time", argPos)
		return
	}
	return
}

func Parse(c Configuration, args []interface{}, targets ...interface{}) error {
	for i := 0; i < len(args) && i < len(targets); i++ {
		trg := targets[i]
		val := args[i]
		switch t := trg.(type) {
		case *[]time.Time:
			_val, err := wantTimes(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *time.Time:
			_val, err := wantTime(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *[]midiline.Condition:
			_val, err := wantStrings(i, val)
			if err != nil {
				return err
			}
			var val []midiline.Condition
			for _, name := range _val {
				mt, errMt := c.FindCondition(name)
				if errMt != nil {
					return errMt
				}
				if mt == nil {
					panic("can't find condition " + name)
				}
				val = append(val, mt)
			}
			*t = val
		case *midiline.Condition:
			_val, err := wantString(i, val)
			if err != nil {
				return err
			}
			mt, err2 := c.FindCondition(_val)
			if err2 != nil {
				return err2
			}
			if mt == nil {
				panic("can't find condition " + _val)
			}
			*t = mt
		case *midiline.Action:
			_val, err := wantString(i, val)
			if err != nil {
				return err
			}
			mt, err2 := c.FindAction(_val)
			if err2 != nil {
				return err2
			}
			if mt == nil {
				panic("can't find action " + _val)
			}
			*t = mt
		case *[]midiline.Action:
			_val, err := wantStrings(i, val)
			if err != nil {
				return err
			}
			var val []midiline.Action
			for _, name := range _val {
				mt, errMt := c.FindAction(name)
				if errMt != nil {
					return errMt
				}
				if mt == nil {
					panic("can't find action " + name)
				}
				val = append(val, mt)
			}
			*t = val
		case *[]*midiline.Line:
			_val, err := wantStrings(i, val)
			if err != nil {
				return err
			}
			var val []*midiline.Line
			for _, name := range _val {
				mt, errMt := c.FindLine(name)
				if errMt != nil {
					return errMt
				}
				if mt == nil {
					panic("can't find line " + name)
				}
				val = append(val, mt)
			}
			*t = val
		case *midiline.Line:
			_val, err := wantString(i, val)
			if err != nil {
				return err
			}
			mt, err2 := c.FindLine(_val)
			if err2 != nil {
				return err2
			}
			if mt == nil {
				panic("can't find line " + _val)
			}
			*t = *mt
		case *int:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *int64:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			*t = int64(_val)
		case *int32:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			*t = int32(_val)
		case *int16:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			*t = int16(_val)
		case *int8:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			*t = int8(_val)
		case *uint:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			if _val < 0 {
				_val = 0
			}
			*t = uint(_val)
		case *uint64:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			if _val < 0 {
				_val = 0
			}
			*t = uint64(_val)
		case *uint32:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			if _val < 0 {
				_val = 0
			}
			*t = uint32(_val)
		case *uint16:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			if _val < 0 {
				_val = 0
			}
			*t = uint16(_val)
		case *uint8:
			_val, err := wantInt(i, val)
			if err != nil {
				return err
			}
			if _val < 0 {
				_val = 0
			}
			*t = uint8(_val)
		case *[]int:
			_val, err := wantInts(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *[]int64:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []int64
			for _, _v := range _vals {
				_val = append(_val, int64(_v))
			}
			*t = _val
		case *[]int32:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []int32
			for _, _v := range _vals {
				_val = append(_val, int32(_v))
			}
			*t = _val
		case *[]int16:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []int16
			for _, _v := range _vals {
				_val = append(_val, int16(_v))
			}
			*t = _val
		case *[]int8:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []int8
			for _, _v := range _vals {
				_val = append(_val, int8(_v))
			}
			*t = _val

		case *[]uint:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []uint
			for _, _v := range _vals {
				if _v < 0 {
					_v = 0
				}
				_val = append(_val, uint(_v))
			}
			*t = _val

		case *[]uint64:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []uint64
			for _, _v := range _vals {
				if _v < 0 {
					_v = 0
				}
				_val = append(_val, uint64(_v))
			}
			*t = _val
		case *[]uint32:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []uint32
			for _, _v := range _vals {
				if _v < 0 {
					_v = 0
				}
				_val = append(_val, uint32(_v))
			}
			*t = _val
		case *[]uint16:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []uint16
			for _, _v := range _vals {
				if _v < 0 {
					_v = 0
				}
				_val = append(_val, uint16(_v))
			}
			*t = _val
		case *[]uint8:
			_vals, err := wantInts(i, val)
			if err != nil {
				return err
			}
			var _val []uint8
			for _, _v := range _vals {
				if _v < 0 {
					_v = 0
				}
				_val = append(_val, uint8(_v))
			}
			*t = _val

		case *string:
			_val, err := wantString(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *[]string:
			_val, err := wantStrings(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *bool:
			_val, err := wantBool(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *[]bool:
			_val, err := wantBools(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *float64:
			_val, err := wantFloat(i, val)
			if err != nil {
				return err
			}
			*t = _val
		case *[]float64:
			_val, err := wantFloats(i, val)
			if err != nil {
				return err
			}
			*t = _val

		case *float32:
			_val, err := wantFloat(i, val)
			if err != nil {
				return err
			}
			*t = float32(_val)

		case *[]float32:
			_vals, err := wantFloats(i, val)
			if err != nil {
				return err
			}
			var _val []float32
			for _, _v := range _vals {
				_val = append(_val, float32(_v))
			}
			*t = _val
		default:
			fmt.Errorf("unknown type %T, supported is a pointer to one of these: bool,string, int*, uint*,float*,time.Time,midiline.Action,midiline.Line,midiline.Condition or to a slice of these.", trg)
		}
	}
	return nil
}
