package config

import (
	"fmt"

	"gitlab.com/gomidi/midiline"
)

type configHandler struct {
	config     *Config
	Conditions map[string]midiline.Condition
	Actions    map[string]midiline.Action
	Lines      map[string]*midiline.Line
}

func (c *configHandler) findConditionInInput(name string) *Condition {
	for _, mt := range c.config.Conditions {
		if mt.Name == name {
			return &mt
		}
	}
	return nil
}

func (c *configHandler) readConditionFromConfig(name string) (midiline.Condition, error) {
	mt := c.findConditionInInput(name)
	if mt == nil {
		return nil, fmt.Errorf("can't find definition of Condition %#v in config", name)
	}

	hd, has := conditionMakerRegistry[mt.Make]
	if !has {
		return nil, fmt.Errorf("unknown Condition maker %#v", mt.Make)
	}

	mmt, err := hd.Condition(c, mt.Params)
	if err != nil {
		return nil, fmt.Errorf("ERROR in Condition %#v: %v", name, err)
	}

	return mmt, nil
}

func (c *configHandler) findActionInInput(name string) *Action {
	for _, tr := range c.config.Actions {
		if tr.Name == name {
			return &tr
		}
	}
	return nil
}

func (c *configHandler) readActionFromConfig(name string) (midiline.Action, error) {
	tr := c.findActionInInput(name)
	if tr == nil {
		return nil, fmt.Errorf("can't find definition of action %#v in config", name)
	}

	hd, has := actionMakerRegistry[tr.Make]
	if !has {
		return nil, fmt.Errorf("unknown action maker %#v", tr.Make)
	}

	ttr, err := hd.Action(c, tr.Params)
	if err != nil {
		return nil, fmt.Errorf("ERROR in action %#v: %v", name, err)
	}

	return ttr, nil
}

func (c *configHandler) findLineInInput(name string) *Line {
	for _, st := range c.config.Lines {
		if st.Name == name {
			return &st
		}
	}
	return nil
}

func (c *configHandler) readLineFromConfig(name string) (*midiline.Line, error) {
	st := c.findLineInInput(name)
	if st == nil {
		return nil, fmt.Errorf("can't find definition of line %#v in config", name)
	}

	var trafos []midiline.Action

	for _, trans := range st.Steps {

		mm, errM := c.FindCondition(trans.Condition)
		if errM != nil {
			return nil, errM
		}

		tr, errTr := c.FindAction(trans.Action)
		if errTr != nil {
			return nil, errTr
		}

		trafos = append(trafos, midiline.Pass(mm, tr))
	}

	return midiline.New(name, trafos...), nil
}

func (c *configHandler) FindCondition(name string) (midiline.Condition, error) {
	if m, has := c.Conditions[name]; has {
		return m, nil
	}

	m, err := c.readConditionFromConfig(name)
	if err != nil {
		return nil, err
	}
	c.Conditions[name] = m
	return m, nil
}

func (c *configHandler) FindAction(name string) (midiline.Action, error) {
	if m, has := c.Actions[name]; has {
		return m, nil
	}

	m, err := c.readActionFromConfig(name)
	if err != nil {
		return nil, err
	}
	c.Actions[name] = m
	return m, nil
}

func (c *configHandler) FindLine(name string) (*midiline.Line, error) {
	if m, has := c.Lines[name]; has {
		return m, nil
	}

	m, err := c.readLineFromConfig(name)
	if err != nil {
		return nil, err
	}
	c.Lines[name] = m
	return m, nil
}
