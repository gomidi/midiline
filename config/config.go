package config

import "gitlab.com/gomidi/midiline"

type Condition struct {
	Name   string        `json="name"`   // the name of the Condition
	Make   string        `json="make"`   // make is a Condition maker and uses params
	Params []interface{} `json="params"` // parameters for the Condition maker
}

type Action struct {
	Name   string        `json="name"`   // the name of the action
	Make   string        `json="make"`   // make is a action maker and uses params
	Params []interface{} `json="params"` // parameters for the action maker
}

// Step is a step in a Line. If the Condition is met, the action is performed.
type Step struct {
	Condition string `json="condition"` // Condition is the name of the Condition, used by the step
	Action    string `json="action"`    // action is the name of a action
}

// the stack with the name "main" will be run
type Line struct {
	Name  string `json="name"`
	Steps []Step `json="steps"`
}

type Config struct {
	Conditions []Condition `json="conditions"`
	Actions    []Action    `json="actions"`
	Lines      []Line      `json="lines"`
}

func (c *Config) Line(name string) (*midiline.Line, error) {
	var cc configHandler
	cc.config = c
	cc.Conditions = map[string]midiline.Condition{}
	cc.Actions = map[string]midiline.Action{}
	cc.Lines = map[string]*midiline.Line{}
	return cc.FindLine(name)
}
