package midiline

import (
	"fmt"
	"io"
	"runtime"
	"time"

	"os"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midi/midimessage/meta"
	"gitlab.com/gomidi/midi/smf"
)

func newPerformanceCtx(in midi.In, out midi.Out, opts ...PerformOption) *performanceCxt {
	ctx := &performanceCxt{}
	ctx.stdout = os.Stdout
	ctx.stderr = os.Stderr
	for _, opt := range opts {
		opt(ctx)
	}

	ctx.out = out
	ctx.in = in
	ctx.rd = mid.NewReader(ctx.readerOpts...)
	ctx.rd.Msg.Each = ctx.gotMessage
	ctx.rd.Msg.Meta.TempoBPM = ctx.gotTempoChange
	return ctx
}

type performanceCxt struct {
	// for now in ticks, but maybe musical time like quarternotes
	// defaults to resolution * 40 (10 4/4 bars)
	//resolution                  uint32
	resolution                  smf.MetricTicks
	projectedch                 chan scheduledMessage
	rd                          *mid.Reader
	readerOpts                  []mid.ReaderOption
	st                          *Line
	closer                      chan bool
	messagePipe                 chan midi.Message
	out                         midi.Out
	in                          midi.In
	tempo                       float64
	tick                        uint64
	readerChannel               chan performanceMsg
	tickDur                     time.Duration
	lastPos                     *mid.Position
	closeTicker                 chan bool
	timeSignature               [2]uint8
	absTicksoflastTimeSigChange uint64
	track                       uint32
	absTickofLastBeatCalc       uint64
	bar                         uint32 // counting from 0
	beat                        uint8  // beat in a bar counting from 0 (in 16th)
	stop                        chan bool
	log                         bool
	stdout                      io.Writer
	stderr                      io.Writer
}

type performanceMsg struct {
	pos *mid.Position
	msg midi.Message
}

func (ctx *performanceCxt) gotTempoChange(pos mid.Position, bpm float64) {
	//fmt.Printf("in perform context: got tempo change: %v\n", bpm)
	ctx.readerChannel <- performanceMsg{&pos, meta.FractionalBPM(bpm)}
}

/*
we need a runner in a separate goroutine, that runs each tick throught the stack,
calculates ticks and beats and bars and updates performance context
and also receives incoming messages by a channel.
we need mutex to protect the performance context when updating ticks, etc.
maybe we can skip all these mutex things if we consider the main part of the performance context
to be this runner and just callback connections to the reader to use only go channels
*/

func (ctx *performanceCxt) gotMessage(pos *mid.Position, msg midi.Message) {
	//fmt.Printf("in perform context: got message: %v\n", msg)
	ctx.readerChannel <- performanceMsg{pos, msg}
	//	ctx.prepareStackRunning(msg)
}

func (ctx *performanceCxt) Bar() uint32 {
	return ctx.bar
}

// in MIDI terms, a beat is a 16th note
// maybe we should use that instead of the
// denominator of time signature
func (ctx *performanceCxt) Beat() uint8 {
	return ctx.beat
}

func (ctx *performanceCxt) Tick() uint64 {
	return ctx.tick
}

func (ctx *performanceCxt) Resolution() uint32 {
	return ctx.rd.Resolution()
}

func (ctx *performanceCxt) TempoBPM() float64 {
	// Tempo comes in via MIDI clock and the mid.Reader
	return ctx.tempo
}

// TODO track could mean "take" in the context of a performance
// and could then be usefully changed, when a Stop message arrives
// it would also be necessity to send a EndOfTrack message for recording.
func (ctx *performanceCxt) Track() uint32 {
	return ctx.track
}

func (ctx *performanceCxt) TimeSig() (num, denom uint8) {
	return ctx.timeSignature[0], ctx.timeSignature[1]
}

func (ctx *performanceCxt) runThroughStack(msg midi.Message) {
	//ctx.runThroughStack(msg)

	// don't do the same tick twice!
	if ctx.tick > ctx.absTickofLastBeatCalc {

		// calc the beat and the bar and then run through the stack

		ticksWithinBar := calcTicksWithinBar(
			ctx.tick, ctx.absTicksoflastTimeSigChange,
			uint64(ctx.resolution),
			uint64(ctx.timeSignature[0]), uint64(ctx.timeSignature[1]))

		if ticksWithinBar == 0 {
			// a bar change just happened, but only
			ctx.bar++
			ctx.beat = 0
		} else {
			ctx.beat = calculateBeat(ticksWithinBar, uint32(ctx.resolution.Resolution()))
		}

		ctx.absTickofLastBeatCalc = ctx.tick
	}

	currentMessages := ctx.st._runThroughLine(ctx, msg)

	for _, msg := range currentMessages {
		if msg == nil {
			fmt.Fprintln(ctx.stderr, "nil messages not allowed, use StackTick instead")
			//			panic("nil messages not allowed, use StackTick instead")
		}

		if msg != LineTick {
			ctx.messagePipe <- msg
		}
	}
}

func (ctx *performanceCxt) _prepareStackRunning(msg midi.Message) {
	if msg == meta.EndOfTrack {
		// not sure, if we should up the track here or based one Stop message
		// ctx.track++
		// we don't remove the message to just be able to distinguish
		// between just tick calls without a message and calls where something happened
		ctx.runThroughStack(msg)
		return
	}

	switch m := msg.(type) {
	case meta.Tempo:
		//fmt.Printf("got tempo: %v\n", m.BPM())
		metric := smf.MetricTicks(ctx.resolution)
		ctx.tickDur = metric.Duration(m.BPM(), 1) // metric.Duration(m.BPM(), 1)
		ctx.tempo = m.FractionalBPM()
	case meta.TimeSig:
		ctx.timeSignature[0] = m.Numerator
		ctx.timeSignature[1] = m.Denominator
		ctx.absTicksoflastTimeSigChange = ctx.tick
	}

	ctx.runThroughStack(msg)

}

/*
func (ctx *performanceCxt) _ticksender(tickCh chan bool) {
	//fmt.Println("ticksender started")
	go func() {
		for {
			select {
			case <-ctx.stop:
				fmt.Fprintln(ctx.stdout, "closing performance context")
				//				fmt.Println("closing performance context")
				ctx.closer <- true
				close(tickCh)
				close(ctx.readerChannel)
				close(ctx.stop)
				return
				//			case <-ctx.closeTicker:
				//				return
			case <-time.After(ctx.tickDur):
				//				fmt.Printf("got tickdur: %v, sleepingns\n", ctx.tickDur.Nanoseconds())
				// sleep until the next tick
				//				time.Sleep(ctx.tickDur)
				//				fmt.Println("now sending tick")
				tickCh <- true
				//			default:
				//			runtime.Gosched()
			}

		}
		//		fmt.Println("ticksender finished, closing channels")
	}()
}
*/

//func (ctx *performanceCxt) _msgBundler(tickCh chan bool) {
func (ctx *performanceCxt) _msgBundler() {
	//fmt.Println("_msgBundler started")
	timer := time.NewTimer(ctx.tickDur)
	go func() {
		for {
			select {
			case <-ctx.stop:
				fmt.Fprintln(ctx.stdout, "closing performance context")
				//				fmt.Println("closing performance context")
				ctx.closer <- true
				//close(tickCh)
				close(ctx.readerChannel)
				close(ctx.stop)
				timer.Stop()
				return

			//case <-tickCh:
			case <-timer.C:
				//fmt.Print(".")
				ctx._prepareStackRunning(LineTick)
			case pm := <-ctx.readerChannel:
				//fmt.Printf("received on readerchannel: %v %v\n", pm.pos, pm.msg)
				ctx.lastPos = pm.pos
				ctx._prepareStackRunning(pm.msg)
			default:
				runtime.Gosched()
			}

		}
		//fmt.Println("_msgBundler finished")
	}()

}

func (ctx *performanceCxt) tickRunner() {
	//fmt.Println("tickruner started")
	//tickCh := make(chan bool, 10)
	//go ctx._msgBundler(tickCh)
	go ctx._msgBundler()
	//go ctx._ticksender(tickCh)
	/*
		go func() {
			for {
				select {
				case <-ctx.closeTicker:
					fmt.Println("closing ticker (from tick runner)")
					close(tickCh)
					return
				case <-tickCh:
					fmt.Print("got tick\n")
					ctx._prepareStackRunning(nil)
				case pm := <-ctx.readerChannel:
					fmt.Printf("received on readerchannel: %v %v\n", pm.pos, pm.msg)
					ctx.lastPos = pm.pos
					ctx._prepareStackRunning(pm.msg)
				//default:
				case <-time.After(ctx.tickDur):
					fmt.Printf("got tickdur: %v, sleepingns\n", ctx.tickDur.Nanoseconds())
					// sleep until the next tick
					//				time.Sleep(ctx.tickDur)
					fmt.Println("woke up, now sending tick")
					tickCh <- true
					//			default:
					//			runtime.Gosched()
				}

			}
			fmt.Println("tickruner finished")
		}()
		fmt.Println("tickruner returned")
	*/
}

func (ctx *performanceCxt) Schedule(deltaticks uint32, msg midi.Message) {
	timestamp := time.Now().Add(ctx.rd.Duration(deltaticks))
	if mm, isMulti := msg.(MultiMessage); isMulti {
		for _, mmm := range mm {
			ctx.projectedch <- scheduledMessage{timestamp, mmm}
		}
	} else {
		ctx.projectedch <- scheduledMessage{timestamp, msg}
	}
}

/*
func metricDurationNanoseconds(t smf.MetricTicks, tempoBPM uint32, deltaTicks uint32) time.Duration {
	// Duration returns the time.Duration for a number of ticks at a certain tempo (in BPM)
	// (60000 / T) * (d / R) = D[ms]
	//	durQnMilli := 60000 / float64(tempoBPM)
	//fmt.Printf("durQnMilli: %v\n", durQnMilli)
	//	_4thticks := float64(deltaTicks) / float64(uint16(t))
	//fmt.Printf("_4thticks: %v\n", _4thticks)

	res := 60000000000 * float64(deltaTicks) / (float64(tempoBPM) * float64(uint16(t)))
	//fmt.Printf("what: %vns\n", res)
	return time.Duration(int64(math.Round(res)))
	//return time.Duration(math.Round(durQnMilli*_4thticks)) * time.Nanosecond
	//	return time.Duration(durQnMilli * _4thticks) // * time.Nanosecond
}
*/

func (ctx *performanceCxt) Run(ln *Line) (stop chan<- bool, err error) {
	err = ln.Check()
	if err != nil {
		return nil, fmt.Errorf("can't run performance, because an error happened during the line check: %v", err)
	}

	if !ctx.in.IsOpen() {
		err = ctx.in.Open()
		if err != nil {
			return nil, fmt.Errorf("can't open in port: %v", err)
		}

	}

	if !ctx.out.IsOpen() {
		err = ctx.out.Open()
		if err != nil {
			return nil, fmt.Errorf("can't open out port: %v", err)
		}

	}

	ctx.projectedch = make(chan scheduledMessage, 10)
	ctx.stop = make(chan bool)
	ctx.closer = make(chan bool)
	ctx.messagePipe = make(chan midi.Message, 10)

	ctx.readerChannel = make(chan performanceMsg, 10)

	ctx.tempo = 120
	ctx.timeSignature = [2]uint8{4, 4}
	ctx.st = ln
	sn := &sender{out: mid.ConnectOut(ctx.out), stdout: ctx.stdout}
	sn.log = ctx.log
	go sn.run(ctx.closer, ctx.messagePipe, ctx.projectedch)
	// TODO we might want to make a reasonable default constant in gomidi/mid, export that and
	// use it here
	ctx.resolution = mid.LiveResolution
	//	metric := smf.MetricTicks(ctx.resolution)
	ctx.tickDur = ctx.resolution.Duration(120, 1) // metric.Duration(120, 1) // default
	//fmt.Printf("starting tickdur: %vns\n", ctx.tickDur.Nanoseconds())
	ctx.tickRunner()
	//	time.Sleep(time.Second)
	//fmt.Println("now sending message")
	go mid.ConnectIn(ctx.in, ctx.rd)
	return ctx.stop, nil
}

var _ Context = &performanceCxt{}
