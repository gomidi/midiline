package channelchange

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/conditions/typ"
	"gitlab.com/gomidi/midiline/config"
)

// Action returns a new channelchange that will route all channel messages from channel from
// to channel to. If from is < 0, it means that any channel will be routed to to.
// If to is < 0, it means that the messages will disappear.
// If from or to are > 15, it will panic because the channel number is invalid (0-15)
func Action(from int8, to int8) midiline.Action {
	if from > 15 {
		panic("invalid midi channel from")
	}
	if to > 15 {
		panic("invalid midi channel to")
	}
	f := []typ.Type{typ.ChannelMsg}
	if from == -1 {
		f = append(f, typ.ChannelAny)
	} else {
		f = append(f, typ.Channel(uint8(from)))
	}
	return midiline.Pass(typ.Join(f...), &channelchange{to: to})
}

type channelchange struct {
	to int8
}

func (r *channelchange) Perform(ctx midiline.Context, msg midi.Message) midi.Message {
	// target < 0 means: filter the message away
	if r.to < 0 {
		return midiline.LineTick
	}
	if m, is := msg.(channel.Message); is {
		return channel.SetChannel(m, uint8(r.to))
	}
	return msg
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "channel.change"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var from, to int8
	err := config.Parse(c, args, &from, &to)
	if err != nil {
		return nil, err
	}

	if from > 15 {
		return nil, fmt.Errorf("invalid source MIDI channel (valid 0-15)")
	}

	if to > 15 {
		return nil, fmt.Errorf("invalid target MIDI channel (valid 0-15)")
	}

	return Action(from, to), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "srcChannel [int], destChannel [int]")
}
