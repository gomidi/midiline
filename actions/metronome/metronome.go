package metronome

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

func Metronome(ch, key, velocity uint8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		if ctx.Beat() == 0 {
			msg := channel.Channel(ch).NoteOn(key, velocity)
			ctx.Schedule(ctx.Resolution(), channel.Channel(ch).NoteOff(key))
			return midiline.MultiMessage{msg, in}
		}
		return in
	})
}

type registryAction struct{}

func (t registryAction) Name() string {
	return "note.metronome"
}

func (t registryAction) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var ch, key, velocity uint8

	err := config.Parse(c, args, &ch, &key, &velocity)
	if err != nil {
		return nil, err
	}

	return Metronome(ch, key, velocity), nil
}

func init() {
	config.RegisterActionMaker(registryAction{}, "channel [int], key [int], velocity [int]")
}
