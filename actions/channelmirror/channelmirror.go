package channelmirror

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

// Action returns a new channelmirror that will mirror all channel messages from channel from
// to channel to. If from or to are > 15, it will panic because the channel number is invalid (0-15)
func Action(from uint8, to uint8) midiline.Action {
	if from > 15 {
		panic("invalid midi channel from")
	}
	if to > 15 {
		panic("invalid midi channel to")
	}
	return &channelmirror{to: to}
}

type channelmirror struct {
	to uint8
}

func (r *channelmirror) getDup(msg midi.Message) midi.Message {
	if m, is := msg.(channel.Message); is {
		return channel.SetChannel(m, r.to)
	}
	return nil
}

func (r *channelmirror) Perform(ctx midiline.Context, msg midi.Message) midi.Message {
	if dup := r.getDup(msg); dup != nil {
		return midiline.MultiMessage([]midi.Message{msg, dup})
	}
	return msg
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "channel.mirror"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var from, to uint8
	err := config.Parse(c, args, &from, &to)
	if err != nil {
		return nil, err
	}

	return Action(from, to), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "srcChannel [int], mirrorChannel [int]")
}
