package copy

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

// Copy calls the action and returns a multimessage
// of the original message and the returned message (if the returned message is a multimessage, it will be
// unpacked first).
func Copy(tr midiline.Action) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		var m = midiline.MultiMessage{in}
		n := tr.Perform(ctx, in)
		if mm, isMulti := n.(midiline.MultiMessage); isMulti {
			m = append(m, mm...)
			return m
		}
		return append(m, n)
	})
}

type registryCopy struct{}

func (t registryCopy) Name() string {
	return "action.copy"
}

func (t registryCopy) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var trafo midiline.Action

	err := config.Parse(c, args, &trafo)
	if err != nil {
		return nil, err
	}

	return Copy(trafo), nil
}

func init() {
	config.RegisterActionMaker(registryCopy{}, "action [string]")
}
