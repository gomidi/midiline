package outbreak

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
	"gitlab.com/gomidi/rtmididrv"
)

type outbreak struct {
	midi.Writer
	duplicate bool
}

func (o *outbreak) Perform(ctx midiline.Context, in midi.Message) midi.Message {
	o.Writer.Write(in)
	if o.duplicate {
		return in
	}
	return midiline.LineTick
}

type Option func(*outbreak)

// Duplicate keeps the message in the stack and just passes a copy
// to the outside writer (by default the message disappears from the stack)
func Duplicate() Option {
	return func(o *outbreak) {
		o.duplicate = true
	}
}

// Action returns a new outbreak that passes the receiving midi messages to another
// midi.Writer (which may be a SMF file, a writer to a mid.OutConnection etc)
func Action(wr midi.Writer, options ...Option) midiline.Action {
	o := &outbreak{Writer: wr}
	for _, opt := range options {
		opt(o)
	}

	return o
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "port.out.copy"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var midiOutPort int

	err := config.Parse(c, args, &midiOutPort)
	if err != nil {
		return nil, err
	}

	drv, err := rtmididrv.New()

	if err != nil {
		return nil, err
	}

	outs, err := drv.Outs()

	if err != nil {
		return nil, err
	}

	if midiOutPort > len(outs)-1 {
		return nil, fmt.Errorf("MIDI out port %v is not available", midiOutPort)
	}
	out := outs[midiOutPort]

	err = out.Open()
	if err != nil {
		return nil, fmt.Errorf("can't open rtmidi out port %d", midiOutPort)
	}

	wr := mid.ConnectOut(out)
	return Action(wr), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "outport [int]")
}
