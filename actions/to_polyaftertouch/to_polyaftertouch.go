package to_polyphonicaftertouch

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
	"gitlab.com/gomidi/midiline/value"
)

// ToControlChange converts the given valuer to a polyphonic after touch message if the given matcher matches.
//
// Example:
//
//    // To get a transformer that converts MIDI notes between 40 and 50 on channel2 to polyphonic aftertouch messages on key 23, use
//    var m = midiline.And(typ.Channel2,message.NoteKeyRange(40,50,true))
//    var t = to_polyphonicaftertouch.Transform(23, m, value.NoteKey)
func Action(key uint8, valuer midiline.Valuer) midiline.Action {
	return ActionScale(key, valuer, nil)
}

// TransformScale is like Transform but allows a scaler function to calculate the message
// value from the valuer value.
//
// Example:
//
//    // To get a transformer that converts MIDI note 64 on channel2 to polyphonic aftertouch message on key 23 with value of 76, use
//    var m = midiline.And(typ.Channel2,message.NoteKey(64,true))
//    var t = TransformScale(23, m, value.NoteKey, func (in uint8) (out uint8){ return in+12 })
func ActionScale(key uint8, valuer midiline.Valuer, scaler func(uint8) uint8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		if !valuer.IsMet(in) {
			return in
		}

		val := valuer.Value(in)

		if scaler != nil {
			val = scaler(val)
		}

		var ch = channel.Channel0

		if chMsg, is := in.(channel.Message); is {
			ch = channel.Channel(chMsg.Channel())
		}

		return ch.PolyAftertouch(key, val)
	})
}

// MapToNote maps the incoming note on/note off messages to polyphonic after touch messages, where
// channel and key corresponds are the same and the velocity of the note on message becomes the value.
// Note off messages send a value of 0.
func MapToNote() midiline.Action {
	return MapToNoteScale(nil)
}

// MapToNoteScale is like MapToNote, but calls the scaler function to convert velocity to after touch value.
func MapToNoteScale(scaler func(uint8) uint8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		switch nt := in.(type) {
		case channel.NoteOn:
			val := nt.Velocity()
			if scaler != nil {
				val = scaler(val)
			}
			return channel.Channel(nt.Channel()).PolyAftertouch(nt.Key(), val)
		case channel.NoteOff:
			var val uint8
			if scaler != nil {
				val = scaler(val)
			}
			return channel.Channel(nt.Channel()).PolyAftertouch(nt.Key(), val)
		case channel.NoteOffVelocity:
			var val uint8
			if scaler != nil {
				val = scaler(val)
			}
			return channel.Channel(nt.Channel()).PolyAftertouch(nt.Key(), val)
		default:
			return in
		}
	})
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "value.to.polyaftertouch"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var valuer string
	var key uint8
	err := config.Parse(c, args, &key, &valuer)
	if err != nil {
		return nil, err
	}

	v := value.FindValuer(valuer)

	if v == nil {
		return nil, fmt.Errorf("%#v is not a value.condition", valuer)
	}

	return Action(key, v), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "key [int], value.condition [string]")
}
