package channelrouter

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
)

// If a channel has no matching transformer, the message will be passed through
type Router map[uint8]midiline.Action

var _ midiline.Action = Router{}

func (r Router) Perform(ctx midiline.Context, in midi.Message) midi.Message {
	cm := in.(channel.Message)
	tr, has := r[cm.Channel()]
	if !has {
		return in
	}
	return tr.Perform(ctx, in)
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "channel.router"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var r = Router{}

	if len(args) < 1 {
		return nil, fmt.Errorf("missing argument at position 0")
	}

	m, ok := args[0].(map[float64]string)
	if !ok {
		return nil, fmt.Errorf("wrong argument at position 0, must be type map[int]string")
	}

	for k, v := range m {
		in, ok := config.ToInt(k)
		if !ok {
			return nil, fmt.Errorf("wrong MIDI channel %v (must be int 0-15)", k)
		}
		if in > 15 || in < 0 {
			return nil, fmt.Errorf("wrong MIDI channel %v (must be 0-15)", in)
		}
		var trafo midiline.Action
		err := config.Parse(c, []interface{}{v}, &trafo)
		if err != nil {
			return nil, fmt.Errorf("target for channel %v is no transformer", k)
		}

		r[uint8(in)] = trafo
	}

	return r, nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "{ channel [int]: action [string]}")
}
