package to_programchange

import (
	"fmt"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/config"
	"gitlab.com/gomidi/midiline/value"
)

// Transform converts the given valuer to a program change if the given matcher matches.
//
// Example:
//
//    // To get a transformer that converts MIDI notes between 40 and 50 on channel2 to program change messages, use
//    var m = midiline.And(typ.Channel2,message.NoteKeyRange(40,50,true))
//    var t = Transform(m, value.NoteKey)
func Action(valuer midiline.Valuer) midiline.Action {
	return ActionScale(valuer, nil)
}

// TransformScale is like Transform but allows a scaler function to calculate the program
// value from the valuer value.
//
// Example:
//
//    // To get a transformer that converts MIDI note 64 on channel2 to a program change 4 message, use
//    var m = midiline.And(typ.Channel2,message.NoteKey(64,true))
//    var t = TransformScale(m, value.NoteKey, func (in uint8) (out uint8){ return in-60 })
func ActionScale(valuer midiline.Valuer, scaler func(uint8) uint8) midiline.Action {
	return midiline.ActionFunc(func(ctx midiline.Context, in midi.Message) midi.Message {
		if !valuer.IsMet(in) {
			return in
		}

		val := valuer.Value(in)

		if scaler != nil {
			val = scaler(val)
		}

		var ch = channel.Channel0

		if chMsg, is := in.(channel.Message); is {
			ch = channel.Channel(chMsg.Channel())
		}

		return ch.ProgramChange(val)
	})
}

type registryTransformer struct{}

func (t registryTransformer) Name() string {
	return "value.to.programchange"
}

func (t registryTransformer) Action(c config.Configuration, args []interface{}) (midiline.Action, error) {
	var valuer string
	err := config.Parse(c, args, &valuer)
	if err != nil {
		return nil, err
	}

	v := value.FindValuer(valuer)

	if v == nil {
		return nil, fmt.Errorf("%#v is not a value.condition", valuer)
	}

	return Action(v), nil
}

func init() {
	config.RegisterActionMaker(registryTransformer{}, "value.condition [string]")
}
