package value

import (
	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
	"gitlab.com/gomidi/midiline"
	"gitlab.com/gomidi/midiline/conditions/typ"
	"gitlab.com/gomidi/midiline/config"
)

func FindValuer(name string) midiline.Valuer {
	switch name {
	case "value.int":
		return Value(0)
	case "value.note.key":
		return NoteKey()
	case "value.note.velocity":
		return NoteVelocity()
	case "value.controlchange":
		return ControlChange()
	case "value.programchange":
		return ProgramChange()
	case "value.aftertouch":
		return Aftertouch()
	case "value.polyaftertouch":
		return PolyAftertouch()
	case "value.polyaftertouch.key":
		return PolyAftertouchKey()
	case "value.pitchbend":
		return Pitchbend()
	default:
		return nil
	}
}

type value uint8

func (value) Name() string {
	return "value.int"
}

func (n value) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	var val uint8
	err := config.Parse(c, args, &val)
	if err != nil {
		return nil, err
	}
	return Value(val), nil
}

func (value) IsMet(msg midi.Message) bool {
	return true
}

func (v value) Value(msg midi.Message) uint8 {
	return uint8(v)
}

func Value(i uint8) midiline.Valuer {
	return value(i)
}

func init() {
	config.RegisterConditionMaker(value(0), "value [uint8]")
}

type noteKey struct{}

func (noteKey) Name() string {
	return "value.note.key"
}

func init() {
	config.RegisterConditionMaker(noteKey{}, "")
}

func (n noteKey) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (noteKey) IsMet(msg midi.Message) bool {
	return typ.Join(typ.NoteMsg, typ.ChannelAny).IsMet(msg)
}

func (noteKey) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.NoteOn:
		return m.Key()
	case channel.NoteOff:
		return m.Key()
	case channel.NoteOffVelocity:
		return m.Key()
	default:
		return 0
	}
}

func NoteKey() midiline.Valuer {
	return noteKey{}
}

type noteVelocity struct{}

func (noteVelocity) Name() string {
	return "value.note.velocity"
}

func init() {
	config.RegisterConditionMaker(noteVelocity{}, "")
}

func (n noteVelocity) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (noteVelocity) IsMet(msg midi.Message) bool {
	return typ.Join(typ.NoteMsg, typ.ChannelAny).IsMet(msg)
}

func (noteVelocity) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.NoteOn:
		return m.Velocity()
	default:
		return 0
	}
}

func NoteVelocity() midiline.Valuer {
	return noteVelocity{}
}

type controlChange struct{}

func (controlChange) Name() string {
	return "value.controlchange"
}

func init() {
	config.RegisterConditionMaker(controlChange{}, "")
}

func (n controlChange) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (controlChange) IsMet(msg midi.Message) bool {
	return typ.Join(typ.ControlChangeMsg, typ.ChannelAny).IsMet(msg)
}

func (controlChange) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.ControlChange:
		return m.Value()
	default:
		return 0
	}
}

func ControlChange() midiline.Valuer {
	return controlChange{}
}

type programChange struct{}

func (programChange) Name() string {
	return "value.programchange"
}

func init() {
	config.RegisterConditionMaker(programChange{}, "")
}

func (n programChange) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (programChange) IsMet(msg midi.Message) bool {
	return typ.Join(typ.ProgramChangeMsg, typ.ChannelAny).IsMet(msg)
}

func (programChange) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.ProgramChange:
		return m.Program()
	default:
		return 0
	}
}

func ProgramChange() midiline.Valuer {
	return programChange{}
}

type aftertouch struct{}

func (aftertouch) Name() string {
	return "value.aftertouch"
}

func init() {
	config.RegisterConditionMaker(aftertouch{}, "")
}

func (n aftertouch) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (aftertouch) IsMet(msg midi.Message) bool {
	return typ.Join(typ.AftertouchMsg, typ.ChannelAny).IsMet(msg)
}

func (aftertouch) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.Aftertouch:
		return m.Pressure()
	default:
		return 0
	}
}

func Aftertouch() midiline.Valuer {
	return aftertouch{}
}

type polyaftertouch struct{}

func (polyaftertouch) Name() string {
	return "value.polyaftertouch"
}

func init() {
	config.RegisterConditionMaker(polyaftertouch{}, "")
}

func (n polyaftertouch) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (polyaftertouch) IsMet(msg midi.Message) bool {
	return typ.Join(typ.PolyAftertouchMsg, typ.ChannelAny).IsMet(msg)
}

func (polyaftertouch) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.PolyAftertouch:
		return m.Pressure()
	default:
		return 0
	}
}

func PolyAftertouch() midiline.Valuer {
	return polyaftertouch{}
}

type polyaftertouchkey struct{}

func (polyaftertouchkey) Name() string {
	return "value.polyaftertouch.key"
}

func init() {
	config.RegisterConditionMaker(polyaftertouchkey{}, "")
}

func (n polyaftertouchkey) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (polyaftertouchkey) IsMet(msg midi.Message) bool {
	return typ.Join(typ.PolyAftertouchMsg, typ.ChannelAny).IsMet(msg)
}

func (polyaftertouchkey) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.PolyAftertouch:
		return m.Key()
	default:
		return 0
	}
}

func PolyAftertouchKey() midiline.Valuer {
	return polyaftertouchkey{}
}

type pitchbend struct{}

func (pitchbend) Name() string {
	return "value.pitchbend"
}

func init() {
	config.RegisterConditionMaker(pitchbend{}, "")
}

func (n pitchbend) Condition(c config.Configuration, args []interface{}) (midiline.Condition, error) {
	return n, nil
}

func (pitchbend) IsMet(msg midi.Message) bool {
	return typ.Join(typ.PitchbendMsg, typ.ChannelAny).IsMet(msg)
}

func (pitchbend) Value(msg midi.Message) uint8 {
	converter := func(in int16) uint8 {
		res := 64 + in
		if res > 127 {
			res = 127
		}
		if res < 0 {
			res = 0
		}
		return uint8(res)
	}
	return PitchbendConvert(converter).Value(msg)
}

func Pitchbend() midiline.Valuer {
	return pitchbend{}
}

type pitchbendConvert struct {
	converter func(int16) uint8
}

func (*pitchbendConvert) IsMet(msg midi.Message) bool {
	return typ.Join(typ.PitchbendMsg, typ.ChannelAny).IsMet(msg)
}

func (p *pitchbendConvert) Value(msg midi.Message) uint8 {
	switch m := msg.(type) {
	case channel.Pitchbend:
		return p.converter(m.Value())
	default:
		return 0
	}
}

func PitchbendConvert(converter func(int16) uint8) midiline.Valuer {
	return &pitchbendConvert{converter}
}
