module gitlab.com/gomidi/midiline

go 1.14

replace (
	gitlab.com/gomidi/midiline/actions/channelchange => ./actions/channelchange
	gitlab.com/gomidi/midiline/conditions/message => ./conditions/message
	gitlab.com/gomidi/midiline/conditions/typ => ./conditions/typ
	gitlab.com/gomidi/midiline/value => ./value
)

require (
	gitlab.com/gomidi/midi v1.15.3
	gitlab.com/gomidi/rtmididrv v0.9.3
)
