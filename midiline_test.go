package midiline

/*
import (
	"bytes"
	"fmt"
	"github.com/gomidi/midi"
	"github.com/gomidi/midi/midimessage/channel"
	"io"
	"testing"
)

type logHandler struct {
	bytes.Buffer
}

func noteToCC(in midi.Message, wr midi.Writer) (err error) {
	switch v := in.(type) {
	case channel.NoteOn:
		err = wr.Write(channel.Channel(v.Channel()).ControlChange(v.Key(), v.Velocity()))
	case channel.NoteOff:
	default:
		err = wr.Write(in)
	}
	return
}


type noop struct{}

func (n noop) Wrap(next Handler) Handler {
	return func(in midi.Message, wr midi.Writer) (err error) {
		fmt.Println("noop Wrap called")
		return next(in, wr)
	}
}

func (l *logHandler) Wrap(next Handler) Handler {
	return func(in midi.Message, wr midi.Writer) (err error) {
		err = next(in, wr)
		fmt.Fprintf(&l.Buffer, "wrapping handler %T:  (error: %v)\n", next,
			err)
		return
	}
}

type trackDest struct {
	bf bytes.Buffer
}

var _ midi.Writer = &trackDest{}

func (t *trackDest) Write(msg midi.Message) error {
	fmt.Fprintf(&t.bf, "got %s\n", msg)
	return nil
}

type fakeSource struct {
	msgs    []midi.Message
	pointer int
}

func newFakeSource(msgs ...midi.Message) midi.Reader {
	return &fakeSource{msgs: msgs, pointer: 0}
}

var _ midi.Reader = &fakeSource{}

func (f *fakeSource) Read() (midi.Message, error) {
	if f.pointer >= len(f.msgs) {
		return nil, io.EOF
	}

	defer func() {
		f.pointer++
	}()

	return f.msgs[f.pointer], nil
}

func TestRun(t *testing.T) {
	var log logHandler
	src := newFakeSource(channel.Channel0.NoteOn(65, 120), channel.Channel0.NoteOff(65))
	var dest trackDest
	//err := New(log.Wrap, (noop{}).Wrap).Run(src, &dest)
	err := New(log.Wrap, (noop{}).Wrap).Run(src, &dest)

	fmt.Println("Error: " + err.Error())
	fmt.Println("log.Buffer: " + log.Buffer.String())
	fmt.Println("dest.bf: " + dest.bf.String())
}

*/
