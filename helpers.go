package midiline

import (
	"time"

	"gitlab.com/gomidi/midi"
	"gitlab.com/gomidi/midi/midimessage/channel"
)

func calcAbsBeat(tick, resolution uint64) uint64 {
	return tick / resolution
}

// calculateBeat returns the distance from start of the the current bar in 16th
func calculateBeat(ticksWithinBar uint64, resolution uint32) uint8 {
	return uint8(ticksWithinBar * 4 / (uint64(resolution) * uint64(16)))
}

func calcTicksWithinBar(tick, absTicksoflastTimeSigChange, resolution, num, denom uint64) uint64 {
	sinceTSchange := tick - absTicksoflastTimeSigChange
	ticksOfABar := resolution * num * denom / 4
	return sinceTSchange % ticksOfABar
}

type scheduledTickMessages []scheduledTickMessage

func (s scheduledTickMessages) Len() int {
	return len(s)
}

func (s scheduledTickMessages) Less(a, b int) bool {
	return s[a].abstick < s[b].abstick
}

func (s scheduledTickMessages) Swap(a, b int) {
	s[a], s[b] = s[b], s[a]
}

type scheduledTickMessage struct {
	msg     midi.Message
	abstick uint64
}

type scheduledMessage struct {
	timestamp time.Time
	msg       midi.Message
}

type scheduleMessages []scheduledMessage

func (p scheduleMessages) Len() int {
	return len(p)
}

func (p scheduleMessages) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p scheduleMessages) Less(i, j int) bool {
	return p[i].timestamp.Before(p[j].timestamp)
}

// isNoteOff returns wether the msg is a note off and
// returns the channel and the key. As note offs are considered:
// midi.NoteOff messages and midi.NoteOn messages with a
// velocity of 0
func isNoteOff(msg midi.Message) (ch, key uint8, is bool) {
	if off, is := msg.(channel.NoteOff); is {
		return off.Channel(), off.Key(), true
	}
	if on, is := msg.(channel.NoteOn); is && on.Velocity() == 0 {
		return on.Channel(), on.Key(), true
	}
	return 0, 0, false
}

// func wants(group MessageFilter, single MessageFilter) bool { return group&single != 0 }
