package midiline

import (
	"io"

	"gitlab.com/gomidi/midi/mid"
)

type PerformOption func(*performanceCxt)

// PerformReaderOptions passes the given options (set/unset logging etc.)
// through to the mid.Reader that receives the midi messages before running through the Stack.
func PerformReaderOptions(opts ...mid.ReaderOption) PerformOption {
	return func(s *performanceCxt) {
		s.readerOpts = opts
	}
}

func StdOut(w io.Writer) PerformOption {
	return func(s *performanceCxt) {
		s.stdout = w
	}
}

func StdErr(w io.Writer) PerformOption {
	return func(s *performanceCxt) {
		s.stderr = w
	}
}

func LogOutput() PerformOption {
	return func(s *performanceCxt) {
		s.log = true
	}
}

/*
type PerformSlimOption func(*performanceCxtSlim)

// PerformReaderOptions passes the given options (set/unset logging etc.)
// through to the mid.Reader that receives the midi messages before running through the Stack.
func PerformReaderOptions(opts ...mid.ReaderOption) PerformSlimOption {
	return func(s *performanceCxtSlim) {
		s.readerOpts = opts
	}
}

func StdOut(w io.Writer) PerformSlimOption {
	return func(s *performanceCxtSlim) {
		s.stdout = w
	}
}

func StdErr(w io.Writer) PerformSlimOption {
	return func(s *performanceCxtSlim) {
		s.stderr = w
	}
}

func LogOutput() PerformSlimOption {
	return func(s *performanceCxtSlim) {
		s.log = true
	}
}
*/

/*
// Latency sets the hopefully constant latency in microseconds
func Latency(microseconds uint32) Option {
	return func(s *Stack) {
		s.latency = microseconds
	}
}
*/
